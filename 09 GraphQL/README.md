# GraphQL

Celem ćwiczenia jest przygotowanie podstawowej wersji projektu w GraphQL



# Ćwiczenia

0. Zainstaluj odpowiednie pakiety do Django oraz GraphQL,
1. Stwórz nowy projekt w Django,
2. Przygotuj co najmniej 3 modele połączone kluczami obcymi - można się posiłkować aplikacją REST,
3. Zmigruj modele na bazę zewnętrzną,
4. Przygotuj plik schema z odpowiednimi klasami opartymi o DjangoObjectType,
5. Przygotuj klasę Query wyświetlającą dane,
6. Przygotuj mutację dla poszczególnych modeli obsługujące dodawanie, modyfikację i usuwanie danych,
7. Dodaj ścieżkę dla GraphQL w pliku urls.py,
8. Zarejestruj modele w pliku admin.py,
9. Stwórz superusera,
10. Dodaj przykładowe dane z poziomu admina,
11. Przygotuj 5 przykładowych zapytań, w wykorzystaniem danych z wielu modeli,
12. Stwórz 5 przykładowych mutacji.
