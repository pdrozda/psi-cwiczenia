# Autentykacja, Testowanie

## Zadania

1. Do wybranych dwóch modeli dodaj właściciela, jako użytkownika wbudowanego w Django
2. Ustaw różne opcje praw dostępu dla poszczególnych modeli - tak, aby były w miarę sensowne
3. Przy dodawaniu rekordów do modeli z właścicielem - ustaw automatycznie właściciela jako użytkownika, który dany rekord stworzył
4. Dodaj widoki dla użytkowników bez możliwości modyfikacji
5. Zadania dotyczące testowania w oparciu o POSTMANa:
   - Stwórz kolekcję, która będzie zawierała Requesty "GET, POST, PUT, DELETE"  
   - Użyj zmiennych lokalnych, globalnych i środowiskowych  
   - Stwórz testy, które będą pobierały i porównywały wcześniej stworzone zmienne w testach  
   - Za pomocą testów stwórz i zapisz nowe zmienne globalne, lokalne i środowiskowe.  
   - Stwórz Testy , które będą sprawdzały:  
       * kod odpowiedzi (201 lub 202)  
       * Czas odpowiedzi mniejszy niż (np. 300ms)  
       * Status odpowiedzi "OK"  
       * Content type 

